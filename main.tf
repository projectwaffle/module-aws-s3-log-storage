# Locals

locals {
  bucket_arn               = "${element(concat(list(format("arn:aws:s3:::%s-%s", var.name, var.resource_identities["bucket"])), list("")), 0)}"
  bucket_id                = "${element(concat(aws_s3_bucket.main.*.id, list("")), 0)}"
  iam_policy_document_main = "${element(concat(data.aws_iam_policy_document.alb.*.json, data.aws_iam_policy_document.cloudtrail.*.json, list("")), 0)}"
}

# S3 Bucket

resource "aws_s3_bucket" "main" {
  count = "${var.create_bucket ? 1 : 0}"

  bucket        = "${format("%s-%s", var.name, var.resource_identities["bucket"])}"
  force_destroy = "${var.force_destroy}"

  versioning {
    enabled = "${var.versioning_enabled}"
  }

  lifecycle_rule {
    id      = "${format("%s-%s", var.name, var.resource_identities["bucket"])}"
    enabled = "${var.lifecycle_rule_enabled}"

    prefix = "${var.lifecycle_rule_prefix}"
    tags   = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["bucket"],)), var.global_tags, var.module_tags, var.lifecycle_rule_tags)}"

    noncurrent_version_expiration {
      days = "${var.noncurrent_version_expiration_days}"
    }

    noncurrent_version_transition {
      days          = "${var.noncurrent_version_transition_days}"
      storage_class = "GLACIER"
    }

    transition {
      days          = "${var.standard_transition_days}"
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = "${var.glacier_transition_days}"
      storage_class = "GLACIER"
    }

    expiration {
      days = "${var.expiration_days}"
    }
  }

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["bucket"],)), var.global_tags, var.module_tags, var.bucket_tags)}"
}

resource "aws_s3_bucket_policy" "main" {
  count = "${var.create_bucket ? 1 : 0}"

  bucket = "${local.bucket_id}"
  policy = "${local.iam_policy_document_main}"
}

# IAM Policy Documents

data "aws_elb_service_account" "main" {}

data "aws_iam_policy_document" "alb" {
  count = "${var.create_bucket &&  var.log_storage_service == "alb" ? 1 : 0}"

  statement {
    sid       = "DefaultOverwritePutObject"
    effect    = "Allow"
    actions   = ["s3:PutObject"]
    resources = ["${local.bucket_arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["${data.aws_elb_service_account.main.arn}"]
    }
  }
}

data "aws_iam_policy_document" "cloudtrail" {
  count = "${var.create_bucket &&  var.log_storage_service == "cloudtrail" ? 1 : 0}"

  statement {
    sid       = "DefaultOverwriteGetBucketAcl"
    effect    = "Allow"
    actions   = ["s3:GetBucketAcl"]
    resources = ["${local.bucket_arn}"]

    principals {
      type        = "service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }

  statement {
    sid       = "DefaultOverwritePutObject"
    effect    = "Allow"
    actions   = ["s3:PutObject"]
    resources = ["${local.bucket_arn}/*"]

    principals {
      type        = "service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = "bucket-owner-full-control"
    }
  }
}
