## Global variables

variable "resource_identities" {
  description = "Resource Identity according to PW Arhitecture Name Convention"
  type        = "map"
}

variable "global_tags" {
  description = "Global Tags"
  type        = "map"
}

## Module Specific variables

variable "module_tags" {
  description = "Module Tags"
  default     = {}
}

variable "name" {
  description = "Name for all resources to start with"
}

## Resource variables

# S3 Bucket

variable "create_bucket" {
  description = "Set to false if you do not want to create s3 for module"
  default     = "false"
}

variable "log_storage_service" {
  description = "Bucket Storage user service. Available options - `alb`, `cloudtrail`"
  default     = ""
}

variable "force_destroy" {
  description = "A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error"
  default     = "true"
}

variable "versioning_enabled" {
  description = "A state of versioning"
  default     = "false"
}

variable "bucket_tags" {
  description = "Custome KMS Tags"
  default     = {}
}

# LifeCycle rules

variable "lifecycle_rule_enabled" {
  description = "Specifies lifecycle rule status"
  default     = "false"
}

variable "lifecycle_rule_prefix" {
  description = "Object key prefix identifying one or more objects to which the rule applies."
  default     = ""
}

variable "lifecycle_rule_tags" {
  description = "Custom Lifecycle Rule Tags"
  default     = {}
}

variable "noncurrent_version_expiration_days" {
  description = "(Optional) Specifies when noncurrent object versions expire."
  default     = "90"
}

variable "noncurrent_version_transition_days" {
  description = "(Optional) Specifies when noncurrent object versions transitions"
  default     = "30"
}

variable "standard_transition_days" {
  description = "Number of days to persist in the standard storage tier before moving to the infrequent access tier"
  default     = "30"
}

variable "glacier_transition_days" {
  description = "Number of days after which to move the data to the glacier storage tier"
  default     = "60"
}

variable "expiration_days" {
  description = "Number of days after which to expunge the objects"
  default     = "90"
}
