# AWS S3 Bucket for storing logs Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-aws-s3.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-aws-s3/)

Terraform module which creates S3 Bucket resources on AWS used to store logs from different services like - CloudTrail, CloudFront, S3, LoadBalancer. Will be used for different modules

These types of resources are supported:

* [S3 Bucket](https://www.terraform.io/docs/providers/aws/r/s3_bucket.html)

Sponsored by [Draw.io - the best way to draw AWS diagrams](https://www.draw.io/)

## Usage

```hcl
module "s3_lb" {
  source = "./module-aws-s3-log-storage"

  name = "training-prod"

  resource_identities = {
    bucket = "app-lb-logs"
  }

  global_tags         = "${var.global_tags}"
  create_bucket       = "true"
  log_storage_service = "alb"
}
```

## Resource Identity

In order to add custom names to resources, each module has two variables to be used for this.
* Variable called `name` - this will be used as the base name for each resource in the module
    * `name = "training-prod"` 
* MAP variable called `resource_identities` - used to append specific name to each resoruce
```hcl
  resource_identities = {
    bucket   = "app=lb-logs"
  }
```

Both variables should be declared in module section: 
```hcl
module "config" {
  ......
  name = "training-prod"
  resource_identities = {
    bucket   = "app-lb-logs"
  }
  ......
}  
```

Next is an example of variable used in the Module resoruce names or tgas 'Name':
```hcl
  name  = "${format("%s-%s", var.name, var.resource_identities["bucket"])}"
  tags  = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["bucket"],)), var.global_tags, var.module_tags, var.bucket_tags)}"
```


## Resource Tagging

There are three level of resource tagging declaration
* Global level - declare tagging strategy that will be applied on all the AWS resources

```hcl
variable "global_tags" {
  type = "map"

  default = {
    environment     = "prod"
    role            = "infrastructure"
    version         = "0.1"
    owner           = "Sergiu Plotnicu"
    bu              = "IT"
    customer        = "project waffle"
    project         = "training"
    confidentiality = "open"
    compliance      = "N/A"
    encryption      = "disabled"
  }
}
```

* Module level - taggig that will affect only resources created by the module itself
```hcl
module "s3" {

  module_tags = {
    bu = "security"
  }
  global_tags         = "${var.global_tags}"
}
```

* Resource level - each resource can be tagged with custome tag or tage that will rewrite all the others one

```hcl
variable "bucket_tags" {
  description = "Custome S3 Tags"
  default     = {
    role = "storage"
  }
}
```

Combination of all this variables should cover all the tagging requirements. 
Tags are beeing merged so, resource level ones, will rewrite modules one, that will rewrite global ones.

## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| bucket\_tags | Custome KMS Tags | map | `<map>` | no |
| create\_bucket | Set to false if you do not want to create s3 for module | string | `false` | no |
| expiration\_days | Number of days after which to expunge the objects | string | `90` | no |
| force\_destroy | A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error | string | `true` | no |
| glacier\_transition\_days | Number of days after which to move the data to the glacier storage tier | string | `60` | no |
| global\_tags | Global Tags | map | - | yes |
| lifecycle\_rule\_enabled | Specifies lifecycle rule status | string | `false` | no |
| lifecycle\_rule\_prefix | Object key prefix identifying one or more objects to which the rule applies. | string | `` | no |
| lifecycle\_rule\_tags | Custom Lifecycle Rule Tags | map | `<map>` | no |
| log\_storage\_service | Bucket Storage user service. Available options - `alb`, `cloudtrail` | string | `` | no |
| module\_tags | Module Tags | map | `<map>` | no |
| name | Name for all resources to start with | string | - | yes |
| noncurrent\_version\_expiration\_days | (Optional) Specifies when noncurrent object versions expire. | string | `90` | no |
| noncurrent\_version\_transition\_days | (Optional) Specifies when noncurrent object versions transitions | string | `30` | no |
| resource\_identities | Resource Identity according to PW Arhitecture Name Convention | map | - | yes |
| standard\_transition\_days | Number of days to persist in the standard storage tier before moving to the infrequent access tier | string | `30` | no |
| versioning\_enabled | A state of versioning | string | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| s3\_bucket\_arn | The ARN of the S3 Bucket |
| s3\_bucket\_domain\_name | The Domain Name of the S3 Bucket |
| s3\_bucket\_hosted\_zone\_id | The Hosted Zone ID of the S3 Bucket |
| s3\_bucket\_id | The ID of the S3 Bucket |
| s3\_bucket\_name | The Name of the S3 Bucket |
| s3\_bucket\_region | The Region of the S3 Bucket |
| s3\_bucket\_regional\_domain\_name | The Regional Domain Name of the S3 Bucket |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/)

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


