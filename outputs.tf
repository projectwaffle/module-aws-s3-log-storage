output "s3_bucket_id" {
  description = "The ID of the S3 Bucket"
  value       = "${element(concat(aws_s3_bucket.main.*.id, list("")), 0)}"
}

output "s3_bucket_arn" {
  description = "The ARN of the S3 Bucket"
  value       = "${element(concat(aws_s3_bucket.main.*.arn, list("")), 0)}"
}

output "s3_bucket_name" {
  description = "The Name of the S3 Bucket"
  value       = "${element(concat(aws_s3_bucket.main.*.bucket, list("")), 0)}"
}

output "s3_bucket_domain_name" {
  description = "The Domain Name of the S3 Bucket"
  value       = "${element(concat(aws_s3_bucket.main.*.bucket_domain_name, list("")), 0)}"
}

output "s3_bucket_regional_domain_name" {
  description = "The Regional Domain Name of the S3 Bucket"
  value       = "${element(concat(aws_s3_bucket.main.*.bucket_regional_domain_name, list("")), 0)}"
}

output "s3_bucket_hosted_zone_id" {
  description = "The Hosted Zone ID of the S3 Bucket"
  value       = "${element(concat(aws_s3_bucket.main.*.hosted_zone_id, list("")), 0)}"
}

output "s3_bucket_region" {
  description = "The Region of the S3 Bucket"
  value       = "${element(concat(aws_s3_bucket.main.*.region, list("")), 0)}"
}
